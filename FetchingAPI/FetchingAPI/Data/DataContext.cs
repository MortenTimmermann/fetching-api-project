﻿using System;
using FetchingAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace FetchingAPI.Data
{
    public class DataContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<FacebookCampaign> FacebookCampaigns { get; set; }
        public DbSet<FacebookInsightGenderAndAge> FacebookInsightsGenderAndAge { get; set; }
        public DbSet<FacebookInsightHour> FacebookInsightHours { get; set; }

        public DbSet<FacebookInsightRegion> FacebookInsightRegions { get; set; }
        public DbSet<UserCompany> UserCompanies { get; set; }
        public DbSet<Company> Companies { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Campaign
            builder.Entity<FacebookCampaign>()
                .HasOne<Company>(a => a.Company)
                .WithOne(b => b.FacebookCampaign)
                .HasForeignKey<FacebookCampaign>(c => c.CompanyID);

            //Insights
            builder.Entity<FacebookInsightGenderAndAge>()
                .HasOne<FacebookCampaign>(a => a.FacebookCampaign)
                .WithMany(b => b.FacebookInsightGenderAndAge)
                .HasForeignKey(c => c.FacebookCampaignID);

            builder.Entity<FacebookInsightRegion>()
                .HasOne<FacebookCampaign>(a => a.FacebookCampaign)
                .WithMany(b => b.FacebookInsightRegions)
                .HasForeignKey(c => c.FacebookCampaignID);

            builder.Entity<FacebookInsightHour>()
                .HasOne<FacebookCampaign>(a => a.FacebookCampaign)
                .WithMany(b => b.FacebookInsightHour)
                .HasForeignKey(c => c.FacebookCampaignID);


            //Company
            builder.Entity<ApplicationUser>()
            .HasMany<UserCompany>(a => a.UserCompanies)
            .WithOne(b => b.ApplicationUser);

            builder.Entity<Company>()
                .HasMany<UserCompany>(a => a.UserCompanies)
                .WithOne(b => b.Company);


            //User
            builder.Entity<ApplicationUser>(entity => {
                entity.ToTable(name: "Users");
                entity.Property(c => c.NormalizedEmail)
                    .HasMaxLength(128)
                    .IsRequired();
                entity.Property(c => c.NormalizedUserName)
                    .HasMaxLength(128)
                    .IsRequired();
                entity.Property(c => c.EmailConfirmed).HasConversion<int>();
                entity.Property(c => c.PhoneNumberConfirmed).HasConversion<int>();
                entity.Property(c => c.TwoFactorEnabled).HasConversion<int>();
                entity.Property(c => c.LockoutEnabled).HasConversion<int>();
            });
            builder.Entity<ApplicationRole>(entity => {
                entity.ToTable(name: "Role");
                entity.Property(c => c.NormalizedName)
                    .HasMaxLength(128)
                    .IsRequired();
            });

            builder.Entity<IdentityUserLogin<Guid>>(entity => entity.Property(m => m.LoginProvider).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<Guid>>(entity => entity.Property(m => m.ProviderKey).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<Guid>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityUserRole<Guid>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityUserRole<Guid>>(entity => entity.Property(m => m.RoleId).HasMaxLength(85));
            builder.Entity<IdentityUserToken<Guid>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityUserToken<Guid>>(entity => entity.Property(m => m.LoginProvider).HasMaxLength(85));
            builder.Entity<IdentityUserToken<Guid>>(entity => entity.Property(m => m.Name).HasMaxLength(85));
            builder.Entity<IdentityUserClaim<Guid>>(entity => entity.Property(m => m.Id).HasMaxLength(85));
            builder.Entity<IdentityUserClaim<Guid>>(entity => entity.Property(m => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityRoleClaim<Guid>>(entity => entity.Property(m => m.Id).HasMaxLength(85));
            builder.Entity<IdentityRoleClaim<Guid>>(entity => entity.Property(m => m.RoleId).HasMaxLength(85));
        }
    }
}
