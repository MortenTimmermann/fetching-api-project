﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.Data.EntityFrameworkCore.Metadata;

namespace FetchingAPI.Migrations
{
    public partial class IgnoreListFieldsInsights : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 128, nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<byte[]>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 128, nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 128, nullable: false),
                    EmailConfirmed = table.Column<int>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<int>(nullable: false),
                    TwoFactorEnabled = table.Column<int>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<int>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FacebookAccessToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FacebookCampaigns",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    FacebookID = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Added = table.Column<DateTime>(nullable: false),
                    CompanyID = table.Column<byte[]>(nullable: false),
                    CurrencyForFacebook = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacebookCampaigns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FacebookCampaigns_Companies_CompanyID",
                        column: x => x.CompanyID,
                        principalTable: "Companies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 85, nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<byte[]>(maxLength: 85, nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 85, nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<byte[]>(maxLength: 85, nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 85, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 85, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<byte[]>(maxLength: 85, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<byte[]>(maxLength: 85, nullable: false),
                    RoleId = table.Column<byte[]>(maxLength: 85, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<byte[]>(maxLength: 85, nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 85, nullable: false),
                    Name = table.Column<string>(maxLength: 85, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserCompanies",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    ApplicationUserId = table.Column<byte[]>(nullable: true),
                    CompanyID = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCompanies", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserCompanies_Users_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCompanies_Companies_CompanyID",
                        column: x => x.CompanyID,
                        principalTable: "Companies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FacebookInsightHours",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    FacebookCampaignID = table.Column<byte[]>(nullable: false),
                    Clicks = table.Column<int>(nullable: false),
                    Unique_clicks = table.Column<int>(nullable: false),
                    Cost_Per_1000_Impressions = table.Column<double>(nullable: false),
                    Percent_Ad_Clicks = table.Column<double>(nullable: false),
                    Cost_Per_1000_Reach = table.Column<double>(nullable: false),
                    Cost_Per_Click = table.Column<double>(nullable: false),
                    Cost_Per_Unique_Click = table.Column<double>(nullable: false),
                    Reach = table.Column<int>(nullable: false),
                    Spend = table.Column<double>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Hour = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacebookInsightHours", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FacebookInsightHours_FacebookCampaigns_FacebookCampaignID",
                        column: x => x.FacebookCampaignID,
                        principalTable: "FacebookCampaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FacebookInsightRegions",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    FacebookCampaignID = table.Column<byte[]>(nullable: false),
                    Clicks = table.Column<int>(nullable: false),
                    Unique_clicks = table.Column<int>(nullable: false),
                    Cost_Per_1000_Impressions = table.Column<double>(nullable: false),
                    Percent_Ad_Clicks = table.Column<double>(nullable: false),
                    Cost_Per_1000_Reach = table.Column<double>(nullable: false),
                    Cost_Per_Click = table.Column<double>(nullable: false),
                    Cost_Per_Unique_Click = table.Column<double>(nullable: false),
                    Reach = table.Column<int>(nullable: false),
                    Spend = table.Column<double>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Region = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacebookInsightRegions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FacebookInsightRegions_FacebookCampaigns_FacebookCampaignID",
                        column: x => x.FacebookCampaignID,
                        principalTable: "FacebookCampaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FacebookInsightsGenderAndAge",
                columns: table => new
                {
                    ID = table.Column<byte[]>(nullable: false),
                    FacebookCampaignID = table.Column<byte[]>(nullable: false),
                    Clicks = table.Column<int>(nullable: false),
                    Unique_clicks = table.Column<int>(nullable: false),
                    Cost_Per_1000_Impressions = table.Column<double>(nullable: false),
                    Percent_Ad_Clicks = table.Column<double>(nullable: false),
                    Cost_Per_1000_Reach = table.Column<double>(nullable: false),
                    Cost_Per_Click = table.Column<double>(nullable: false),
                    Cost_Per_Unique_Click = table.Column<double>(nullable: false),
                    Reach = table.Column<int>(nullable: false),
                    Spend = table.Column<double>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    Age = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacebookInsightsGenderAndAge", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FacebookInsightsGenderAndAge_FacebookCampaigns_FacebookCampa~",
                        column: x => x.FacebookCampaignID,
                        principalTable: "FacebookCampaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_FacebookCampaigns_CompanyID",
                table: "FacebookCampaigns",
                column: "CompanyID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FacebookInsightHours_FacebookCampaignID",
                table: "FacebookInsightHours",
                column: "FacebookCampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_FacebookInsightRegions_FacebookCampaignID",
                table: "FacebookInsightRegions",
                column: "FacebookCampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_FacebookInsightsGenderAndAge_FacebookCampaignID",
                table: "FacebookInsightsGenderAndAge",
                column: "FacebookCampaignID");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Role",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserCompanies_ApplicationUserId",
                table: "UserCompanies",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCompanies_CompanyID",
                table: "UserCompanies",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "FacebookInsightHours");

            migrationBuilder.DropTable(
                name: "FacebookInsightRegions");

            migrationBuilder.DropTable(
                name: "FacebookInsightsGenderAndAge");

            migrationBuilder.DropTable(
                name: "UserCompanies");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "FacebookCampaigns");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
