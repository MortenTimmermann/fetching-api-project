﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Output.Company
{
    public class CompanyExtended : Models.Company
    {
        public int Participants { get; set; }
        public bool ConnectedToFacebook { get; set; }
    }
}
