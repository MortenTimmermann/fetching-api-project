﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Output.Facebook
{
    public class CampaignExtendedOutput
    {
        //ID comes from facebook
        public string ID { get; set; }

        public string EncryptedID { get; set; }

        public string Name { get; set; }

        //Created in Facebok
        public DateTime Created { get; set; }

        //Added to DB
        public DateTime Added { get; set; }
    }
}
