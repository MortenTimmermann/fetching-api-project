﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.DataProtection;

namespace FetchingAPI.Services
{
    public class SecurityService
    {
        private readonly IDataProtector _protector;
        private readonly IConfiguration _config;

        public SecurityService(IConfiguration config, IDataProtectionProvider provider)
        {
            this._config = config;
            _protector = provider.CreateProtector(_config["Encryption:Key"]);
        }

        public string Encrypt(string prop)
        {
            return _protector.Protect(prop);
        }
        public string Decrypt(string prop)
        {
            return _protector.Unprotect(prop);
        }
    }
}
