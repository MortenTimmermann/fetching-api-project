﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class Company
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public ICollection<UserCompany> UserCompanies { get; set; }

        public FacebookCampaign FacebookCampaign { get; set; }


        //Inkluder måske - Type: Webshop osv. Ca Omsætning om året. Antal Ansatte osv osv
    }
}
