﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class FacebookInsightParent
    {
        public Guid ID { get; set; }
        public Guid FacebookCampaignID { get; set; }

        public FacebookCampaign FacebookCampaign { get; set; }

        public int Clicks { get; set; }

        public int Unique_clicks { get; set; }

        public double Cost_Per_1000_Impressions { get; set; }

        public double Percent_Ad_Clicks { get; set; }

        public double Cost_Per_1000_Reach { get; set; }

        public double Cost_Per_Click { get; set; }

        public double Cost_Per_Unique_Click { get; set; }

        public int Reach { get; set; }

        public double Spend { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}
