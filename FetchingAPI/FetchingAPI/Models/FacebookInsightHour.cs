﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class FacebookInsightHour : FacebookInsightParent
    {
        public string Hour { get; set; }
    }
}
