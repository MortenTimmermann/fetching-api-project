﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class UserCompany
    {
        public Guid ID { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public Company Company { get; set; }
    }
}
