﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class FacebookInsightGenderAndAge : FacebookInsightParent
    {

        public string Gender { get; set; }

        public string Age { get; set; }


    }
}
