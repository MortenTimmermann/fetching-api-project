﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class FacebookCampaign
    {
        public Guid ID { get; set; }
        public string FacebookID { get; set; }

        public string Name { get; set; }

        //Created in Facebok
        public DateTime Created { get; set; }

        //Added to DB
        public DateTime Added { get; set; }

        public Guid CompanyID { get; set; }

        public Company Company { get; set; }

        public List<FacebookInsightGenderAndAge> FacebookInsightGenderAndAge { get; set; }

        public List<FacebookInsightHour> FacebookInsightHour { get; set; }

        public List<FacebookInsightRegion> FacebookInsightRegions { get; set; }

        public string CurrencyForFacebook { get; set; }
    }
}
