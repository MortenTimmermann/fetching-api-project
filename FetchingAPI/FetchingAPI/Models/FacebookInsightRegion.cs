﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models
{
    public class FacebookInsightRegion : FacebookInsightParent
    {
        public string Region { get; set; }
    }
}
