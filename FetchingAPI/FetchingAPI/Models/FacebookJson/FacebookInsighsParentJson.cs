﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models.FacebookJson
{
    public class FacebookInsighsParentJson
    {
        [Newtonsoft.Json.JsonProperty("clicks")]
        public int Clicks { get; set; }

        [Newtonsoft.Json.JsonProperty("unique_clicks")]
        public int Unique_clicks { get; set; }

        [Newtonsoft.Json.JsonProperty("cpm")]
        public double Cpm { get; set; }

        [Newtonsoft.Json.JsonProperty("ctr")]
        public double Ctr { get; set; }

        [Newtonsoft.Json.JsonProperty("cpp")]
        public double Cpp { get; set; }

        [Newtonsoft.Json.JsonProperty("cpc")]
        public double Cpc { get; set; }

        [Newtonsoft.Json.JsonProperty("reach")]
        public int Reach { get; set; }

        [Newtonsoft.Json.JsonProperty("spend")]
        public double Spend { get; set; }

        [Newtonsoft.Json.JsonProperty("account_currency")]
        public string Currency { get; set; }
    }
}
