﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models.FacebookJson
{
    public class FacebookInsightsRegionsJsonList
    {
        [Newtonsoft.Json.JsonProperty("data")]
        public List<FacebookInsightsRegionsJson> FacebookInsightsOutput { get; set; }
    }
    public class FacebookInsightsRegionsJson : FacebookInsighsParentJson
    {

        [Newtonsoft.Json.JsonProperty("region")]
        public string Region { get; set; }

    }
}
