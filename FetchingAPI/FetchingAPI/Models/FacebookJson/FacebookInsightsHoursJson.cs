﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models.FacebookJson
{

    public class FacebookInsightsHoursJsonList
    {
        [Newtonsoft.Json.JsonProperty("data")]
        public List<FacebookInsightsHoursJson> FacebookInsightsOutput { get; set; }
    }
    public class FacebookInsightsHoursJson : FacebookInsighsParentJson
    {

        [Newtonsoft.Json.JsonProperty("hourly_stats_aggregated_by_audience_time_zone")]
        public string Hour { get; set; }

    }
}
