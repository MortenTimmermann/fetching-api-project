﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Models.FacebookJson
{

    public class FacebookInsightsGenderAndAgeJsonList
    {
        [Newtonsoft.Json.JsonProperty("data")]
        public List<FacebookInsightsGenderAndAgeJson> FacebookInsightsOutput { get; set; }
    }
    public class FacebookInsightsGenderAndAgeJson : FacebookInsighsParentJson
    {
        [Newtonsoft.Json.JsonProperty("gender")]
        public string Gender { get; set; }

        [Newtonsoft.Json.JsonProperty("age")]
        public string Age { get; set; }
    }
}
