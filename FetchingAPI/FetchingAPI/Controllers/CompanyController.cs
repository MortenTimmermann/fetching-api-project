﻿using FetchingAPI.Data;
using FetchingAPI.Input.Company;
using FetchingAPI.Models;
using FetchingAPI.Services;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using FetchingAPI.Output.Company;


namespace FetchingAPI.Controllers
{
    public class CompanyController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _config;
        private readonly DataContext _context;
        private JwtService _jwtService;
        private SecurityService _securityService;

        public CompanyController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, DataContext dataContext, IDataProtectionProvider provider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = configuration;
            _context = dataContext;
            _jwtService = new JwtService(_config);
            _securityService = new SecurityService(_config, provider);
        }

        [HttpPost]
        [Route("api/company/create")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> CreateCompany([FromBody] CreateCompanyInput createCompanyInput)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    //Validate if the name is free to use.
                    Company company = await _context.Companies.SingleOrDefaultAsync(x => x.Name.ToLower() == createCompanyInput.Name.ToLower());
                    if (company != null)
                    {
                        return BadRequest();
                    }

                    company = new Company();
                    company.Name = createCompanyInput.Name;
                    company.Created = DateTime.UtcNow;
                    company.FacebookCampaign = _context.FacebookCampaigns.SingleOrDefault(x => x.Company == company);

                    UserCompany UC = new UserCompany();
                    UC.ApplicationUser = user;
                    UC.Company = company;

                    _context.Add(company);
                    _context.Add(UC);
                    await _context.SaveChangesAsync();

                    user.UserCompanies = new List<UserCompany>();
                    user.UserCompanies.Add(UC);

                    return Json(company);
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("api/company/join")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> JoinCompany([FromBody] JoinCompanyInput joinCompanyInput)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    Company company = await _context.Companies.Include(x => x.FacebookCampaign).Where(x => x.ID == new Guid(joinCompanyInput.ID)).SingleOrDefaultAsync();

                    if (company == null)
                    {
                        return BadRequest();
                    }

                    UserCompany CheckForAlreadyConnected = await _context.UserCompanies.Where(x => x.Company == company && x.ApplicationUser == user).SingleOrDefaultAsync();
                    if (CheckForAlreadyConnected != null)
                    {
                        return BadRequest();
                    }

                    UserCompany UC = new UserCompany();
                    UC.ApplicationUser = user;
                    UC.Company = company;

                    _context.Add(UC);
                    await _context.SaveChangesAsync();

                    user.UserCompanies = new List<UserCompany>();
                    user.UserCompanies.Add(UC);

                    return Json(company);
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }

        [HttpGet]
        [Route("api/company/list")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> CompanyList()
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    List<Company> Companies = await _context.UserCompanies.Include(x => x.Company).Where(x => x.ApplicationUser == user).Select(x => x.Company).ToListAsync();

                    if (Companies == null)
                    {
                        return NotFound();
                    }

                    Companies.ForEach(company =>
                    {
                        company.FacebookCampaign = _context.FacebookCampaigns.SingleOrDefault(x => x.Company == company);
                    });

                    return Json(Companies);
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("api/company/information")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> CompanyInformation([FromBody] CompanyInformationInput companyInformationInput)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    UserCompany CheckForConnection = await _context.UserCompanies.Include(x => x.Company).SingleOrDefaultAsync(x => x.Company.ID == new Guid(companyInformationInput.ID) && x.ApplicationUser == user);
                    if (CheckForConnection == null)
                    {
                        return BadRequest();
                    }

                    Company company = await _context.Companies.Include(x => x.FacebookCampaign).SingleOrDefaultAsync(x => x == CheckForConnection.Company);
                    company.UserCompanies = null;

                    return Json(company);
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }
    }
}
