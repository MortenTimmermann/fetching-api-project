﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FetchingAPI.Controllers
{
    public class GreetController : Controller
    {
        [HttpGet]
        [Route("api/greet")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> greet()
        {
            return Json("Justo Fetching API greets at " + DateTime.Now);
        }
    }
}
