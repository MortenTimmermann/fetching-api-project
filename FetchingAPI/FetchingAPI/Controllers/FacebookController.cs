﻿using FetchingAPI.Data;
using FetchingAPI.Input.Facebook;
using FetchingAPI.Models;
using FetchingAPI.Models.FacebookJson;
using FetchingAPI.Output.Facebook;
using FetchingAPI.Services;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Cors;

namespace FetchingAPI.Controllers
{
    public class FacebookController : Controller
    {
        private static string FacebookUrl = "https://graph.facebook.com/";
        private static string Version = "v8.0/";
        private string BaseUrl = FacebookUrl + Version;


        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _config;
        private readonly DataContext _context;
        private JwtService _jwtService;
        private SecurityService _securityService;

        public FacebookController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, DataContext dataContext, IDataProtectionProvider provider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = configuration;
            _context = dataContext;
            _jwtService = new JwtService(_config);
            _securityService = new SecurityService(_config, provider);
        }
        

        [HttpPost]
        [Route("api/get/campaign/insights")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> getCampaignInsights([FromBody] CampaignInsightsInput campaignInsights)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    if (user.FacebookAccessToken == null)
                    {
                        return BadRequest();
                    }

                    Company company = await _context.Companies.Include(x => x.FacebookCampaign).SingleOrDefaultAsync(x => x.ID == new Guid(campaignInsights.CompanyID));

                    if (company == null)
                    {
                        return NotFound();
                    }

                    FacebookCampaign facebookCampaign = company.FacebookCampaign;

                    facebookCampaign.FacebookInsightGenderAndAge = await _context.FacebookInsightsGenderAndAge.Where(x => x.FacebookCampaign == company.FacebookCampaign).OrderBy(x => x.FromDate).ToListAsync();
                    facebookCampaign.FacebookInsightHour = await _context.FacebookInsightHours.Where(x => x.FacebookCampaign == company.FacebookCampaign).OrderBy(x => x.FromDate).ToListAsync();
                    facebookCampaign.FacebookInsightRegions = await _context.FacebookInsightRegions.Where(x => x.FacebookCampaign == company.FacebookCampaign).OrderBy(x => x.FromDate).ToListAsync();

                    //FacebookInsightsGenderAndAge.ForEach(x => x.FacebookCampaign = null);

                    DateTime to = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                    if (facebookCampaign.FacebookInsightGenderAndAge.Count == 0)
                    {
                        DateTime from = new DateTime(company.FacebookCampaign.Created.Year, company.FacebookCampaign.Created.Month, 1);
                        await getGenderAndAgeData(company, user, from, to, facebookCampaign.FacebookInsightGenderAndAge);
                    }
                    else
                    {
                        FacebookInsightParent latest = facebookCampaign.FacebookInsightGenderAndAge.Last();
                        DateTime from = new DateTime(latest.FromDate.AddMonths(1).Year, latest.FromDate.AddMonths(1).Month, 1);
                        await getGenderAndAgeData(company, user, from, to, facebookCampaign.FacebookInsightGenderAndAge);
                    }


                    if(facebookCampaign.FacebookInsightHour.Count == 0)
                    {
                        DateTime from = new DateTime(company.FacebookCampaign.Created.Year, company.FacebookCampaign.Created.Month, 1);
                        await getHourData(company, user, from, to, facebookCampaign.FacebookInsightHour);
                    }
                    else
                    {
                        FacebookInsightParent latest = facebookCampaign.FacebookInsightHour.Last();
                        DateTime from = new DateTime(latest.FromDate.AddMonths(1).Year, latest.FromDate.AddMonths(1).Month, 1);
                        await getHourData(company, user, from, to, facebookCampaign.FacebookInsightHour);
                    }


                    if (facebookCampaign.FacebookInsightRegions.Count == 0)
                    {
                        DateTime from = new DateTime(company.FacebookCampaign.Created.Year, company.FacebookCampaign.Created.Month, 1);
                        await getRegionData(company, user, from, to, facebookCampaign.FacebookInsightRegions);
                    }
                    else
                    {
                        FacebookInsightParent latest = facebookCampaign.FacebookInsightRegions.Last();
                        DateTime from = new DateTime(latest.FromDate.AddMonths(1).Year, latest.FromDate.AddMonths(1).Month, 1);
                        await getRegionData(company, user, from, to, facebookCampaign.FacebookInsightRegions);
                    }

                    await _context.SaveChangesAsync();
                    return Json(company.FacebookCampaign);
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }


        public async Task getGenderAndAgeData(Company company, ApplicationUser user, DateTime from, DateTime to, List<FacebookInsightGenderAndAge> list)
        {
            while(from.CompareTo(to) < 0)
            {
                DateTime LastDayInMonth = from.AddMonths(1).AddDays(-1);
                UriBuilder builder = new UriBuilder(BaseUrl + "act_" + company.FacebookCampaign.FacebookID + "/insights");
                builder.Query = "breakdowns=age,gender&";
                builder.Query += "fields=reach,cpc,cpm,cpp,ctr,unique_clicks,clicks,social_spend,spend,account_currency&";
                builder.Query += "time_range[since]=" + from.ToString("yyyy-MM-dd") + "&time_range[until]=" + LastDayInMonth.ToString("yyyy-MM-dd") + "&";
                builder.Query += "access_token=" + _securityService.Decrypt(user.FacebookAccessToken);

                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync(builder.Uri).Result;
                    if (response != null)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        FacebookInsightsGenderAndAgeJsonList result = JsonConvert.DeserializeObject<FacebookInsightsGenderAndAgeJsonList>(jsonString);

                        if (result.FacebookInsightsOutput.Count > 0)
                        {
                            result.FacebookInsightsOutput.ForEach(output =>
                            {
                                FacebookInsightGenderAndAge FIGA = new FacebookInsightGenderAndAge();
                                FIGA.ID = new Guid();
                                FIGA.FacebookCampaign = company.FacebookCampaign;
                                FIGA.Clicks = output.Clicks;
                                FIGA.Unique_clicks = output.Unique_clicks;
                                FIGA.Cost_Per_1000_Impressions = output.Cpm;
                                FIGA.Percent_Ad_Clicks = output.Ctr;
                                FIGA.Cost_Per_1000_Reach = output.Cpp;
                                FIGA.Cost_Per_Click = output.Cpc;
                                FIGA.Cost_Per_Unique_Click = output.Unique_clicks > 0 ? output.Spend / output.Unique_clicks : 0;
                                FIGA.Reach = output.Reach;
                                FIGA.Spend = output.Spend;
                                FIGA.FromDate = from;
                                FIGA.ToDate = LastDayInMonth;
                                FIGA.Gender = output.Gender;
                                FIGA.Age = output.Age;
                                list.Add(FIGA);
                                _context.Add(FIGA);
                            });
                        }
                    }
                }
                from = from.AddMonths(1);
            }
        }

        public async Task getRegionData(Company company, ApplicationUser user, DateTime from, DateTime to, List<FacebookInsightRegion> list)
        {
            while (from.CompareTo(to) < 0)
            {
                DateTime LastDayInMonth = from.AddMonths(1).AddDays(-1);
                UriBuilder builder = new UriBuilder(BaseUrl + "act_" + company.FacebookCampaign.FacebookID + "/insights");
                builder.Query = "breakdowns=region&";
                builder.Query += "fields=reach,cpc,cpm,cpp,ctr,unique_clicks,clicks,social_spend,spend,account_currency&";
                builder.Query += "time_range[since]=" + from.ToString("yyyy-MM-dd") + "&time_range[until]=" + LastDayInMonth.ToString("yyyy-MM-dd") + "&";
                builder.Query += "access_token=" + _securityService.Decrypt(user.FacebookAccessToken);

                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync(builder.Uri).Result;
                    if (response != null)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        FacebookInsightsRegionsJsonList result = JsonConvert.DeserializeObject<FacebookInsightsRegionsJsonList>(jsonString);

                        if (result.FacebookInsightsOutput.Count > 0)
                        {
                            result.FacebookInsightsOutput.ForEach(output =>
                            {
                                FacebookInsightRegion FIR = new FacebookInsightRegion();
                                FIR.ID = new Guid();
                                FIR.FacebookCampaign = company.FacebookCampaign;
                                FIR.Clicks = output.Clicks;
                                FIR.Unique_clicks = output.Unique_clicks;
                                FIR.Cost_Per_1000_Impressions = output.Cpm;
                                FIR.Percent_Ad_Clicks = output.Ctr;
                                FIR.Cost_Per_1000_Reach = output.Cpp;
                                FIR.Cost_Per_Click = output.Cpc;
                                FIR.Cost_Per_Unique_Click = output.Unique_clicks > 0 ? output.Spend / output.Unique_clicks : 0;
                                FIR.Reach = output.Reach;
                                FIR.Spend = output.Spend;
                                FIR.FromDate = from;
                                FIR.ToDate = LastDayInMonth;
                                FIR.Region = output.Region;
                                list.Add(FIR);
                                _context.Add(FIR);
                            });
                        }
                    }
                }
                from = from.AddMonths(1);
            }
        }

        public async Task getHourData(Company company, ApplicationUser user, DateTime from, DateTime to, List<FacebookInsightHour> list)
        {
            while (from.CompareTo(to) < 0)
            {
                DateTime LastDayInMonth = from.AddMonths(1).AddDays(-1);
                UriBuilder builder = new UriBuilder(BaseUrl + "act_" + company.FacebookCampaign.FacebookID + "/insights");
                builder.Query = "breakdowns=hourly_stats_aggregated_by_audience_time_zone&";
                builder.Query += "fields=reach,cpc,cpm,cpp,ctr,unique_clicks,clicks,social_spend,spend,account_currency&";
                builder.Query += "time_range[since]=" + from.ToString("yyyy-MM-dd") + "&time_range[until]=" + LastDayInMonth.ToString("yyyy-MM-dd") + "&";
                builder.Query += "access_token=" + _securityService.Decrypt(user.FacebookAccessToken);

                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync(builder.Uri).Result;
                    if (response != null)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        FacebookInsightsHoursJsonList result = JsonConvert.DeserializeObject<FacebookInsightsHoursJsonList>(jsonString);

                        if (result.FacebookInsightsOutput.Count > 0)
                        {
                            result.FacebookInsightsOutput.ForEach(output =>
                            {
                                FacebookInsightHour FIH = new FacebookInsightHour();
                                FIH.ID = new Guid();
                                FIH.FacebookCampaign = company.FacebookCampaign;
                                FIH.Clicks = output.Clicks;
                                FIH.Unique_clicks = output.Unique_clicks;
                                FIH.Cost_Per_1000_Impressions = output.Cpm;
                                FIH.Percent_Ad_Clicks = output.Ctr;
                                FIH.Cost_Per_1000_Reach = output.Cpp;
                                FIH.Cost_Per_Click = output.Cpc;
                                FIH.Cost_Per_Unique_Click = output.Unique_clicks > 0 ? output.Spend / output.Unique_clicks : 0;
                                FIH.Reach = output.Reach;
                                FIH.Spend = output.Spend;
                                FIH.FromDate = from;
                                FIH.ToDate = LastDayInMonth;
                                FIH.Hour = output.Hour;
                                list.Add(FIH);
                                _context.Add(FIH);
                            });
                        }
                    }
                }
                from = from.AddMonths(1);
            }
        }


        [HttpGet]
        [Route("api/get/campaigns")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> getCampaignsFromFacebook()
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if(user == null)
                    {
                        return Unauthorized();
                    }

                    if(user.FacebookAccessToken == null)
                    {
                        return BadRequest();
                    }

                    UriBuilder builder = new UriBuilder(BaseUrl + "me");
                    builder.Query = "fields=adaccounts{name,account_id, created_time, currency}&";
                    builder.Query += "time_range[since]=2019-01-10&time_range[until]=2020-06-09&";
                    builder.Query += "access_token=" + _securityService.Decrypt(user.FacebookAccessToken);

                    using (HttpClient client = new HttpClient())
                    {
                        var response = client.GetAsync(builder.Uri).Result;
                        if (response != null)
                        {
                            var jsonString = await response.Content.ReadAsStringAsync();
                            var result = JsonConvert.DeserializeObject<object>(jsonString);
                            return Json(result);
                        }
                        return BadRequest(response);
                    }
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }
        
        [HttpPost]
        [Route("api/post/campaign")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> subscribeCampaign([FromBody] PostCampaign PostCampaign)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }


                    Company company = await _context.Companies.SingleOrDefaultAsync(x => x.ID == new Guid(PostCampaign.CompanyID));

                    if(company == null)
                    {
                        return NotFound();
                    }

                    FacebookCampaign FC = new FacebookCampaign();
                    FC.FacebookID = PostCampaign.FacebookID;
                    FC.Name = PostCampaign.Name;
                    FC.Created = DateTime.Parse(PostCampaign.Created);
                    FC.Company = company;
                    FC.Added = DateTime.UtcNow;
                    FC.CurrencyForFacebook = PostCampaign.Currency;

                    _context.Add(FC);
                    await _context.SaveChangesAsync();
                    company.FacebookCampaign = FC;

                    //Update the company with the facebook extention
                    return Json(company);
                }
                catch(Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }
    }
}


//Get key here
//https://developers.facebook.com/tools/explorer/

