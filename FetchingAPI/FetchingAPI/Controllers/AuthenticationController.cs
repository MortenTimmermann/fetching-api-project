﻿using System;
using FetchingAPI.Services;
using System.Linq;
using FetchingAPI.Models;
using FetchingAPI.Input.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Web.Http.Cors;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using FetchingAPI.Data;
using Microsoft.AspNetCore.DataProtection;

namespace FetchingAPI.Controllers
{
    public class AuthenticationController : Controller
    {

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _config;
        private readonly DataContext _context;
        private JwtService _jwtService;
        private SecurityService _securityService;

        public AuthenticationController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, DataContext dataContext, IDataProtectionProvider provider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = configuration;
            _context = dataContext;
            _jwtService = new JwtService(_config);
            _securityService = new SecurityService(_config, provider);
        }

        [HttpPost]
        [Route("api/user/login")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> Login([FromBody] LoginEmailDTO model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (result.Succeeded)
            {
                ApplicationUser user = _userManager.Users.Include(x => x.UserCompanies).SingleOrDefault(r => r.Email == model.Email);
                return Json(_jwtService.GenerateJwtToken(user));
            }

            return NotFound(result);
        }

        [HttpPost]
        [Route("api/user/register")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> Register([FromBody] RegisterDTO model)
        {
            var user = new ApplicationUser
            {
                Name = model.Name,
                UserName = model.Email,
                Email = model.Email
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return Json(_jwtService.GenerateJwtToken(user));
            }

            return NotFound(result);
        }

        [HttpGet]
        [Route("api/user/new/token")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> GetNewToken()
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.Include(x => x.UserCompanies).SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }

                    return Json(_jwtService.GenerateJwtToken(user));
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }

        
        [HttpPost]
        [Route("api/user/login/facebook")] 
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IActionResult> LoginSocial([FromBody] LoginFacebookDTO model)
        {
            string token = HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                try
                {
                    ApplicationUser user = await _context.Users.Include(x => x.UserCompanies).SingleOrDefaultAsync(u => u.Email == _jwtService.GetClaimValue(token, "email"));

                    if (user == null)
                    {
                        return Unauthorized();
                    }
                    user.FacebookAccessToken = _securityService.Encrypt(model.FacebookAccessToken);
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                    return Json(_jwtService.GenerateJwtToken(user));
                }
                catch (Exception E)
                {
                    return NotFound(E);
                }
            }
            return Unauthorized();
        }
        

    }
}
