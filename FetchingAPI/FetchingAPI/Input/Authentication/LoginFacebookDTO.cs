﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Input.Authentication
{
    public class LoginFacebookDTO
    {
        public string FacebookAccessToken { get; set; }
    }
}
