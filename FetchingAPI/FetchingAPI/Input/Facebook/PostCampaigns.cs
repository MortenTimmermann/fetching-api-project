﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FetchingAPI.Input.Facebook
{
    public class PostCampaign
    {
        public string FacebookID { get; set; }

        public string CompanyID { get; set; }
        public string Name { get; set; }
        public string Created { get; set; }

        public string Currency { get; set; }
    }
}
